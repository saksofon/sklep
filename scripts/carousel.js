$(document).ready(function() {
 const owlCarouselOptions = {
        loop: true,
        center: false,
        margin: 1,
        stagePadding: 0,
        items: 1,
        pullDrag: false,
        autoplay: false,
        autoplayTimeout: 3000,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right fa-1x" aria-hidden="true"></i>'],
        nav: true,
        lazyLoad: true,
        fluidSpeed: true,
        slideBy: 3,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    };
    $('.slider').owlCarousel(owlCarouselOptions);
    owlCarouselOptions.autoplayTimeout = 2000;
        owlCarouselOptions.autoplay = false;
         // owlCarouselOptions.nav = false;
    owlCarouselOptions.responsive = {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        1000: {
            items: 4
        }
    };
    $('.slider-2').owlCarousel(owlCarouselOptions);
    owlCarouselOptions.margin = 0;
    owlCarouselOptions.autoplayTimeout = 2000;
    owlCarouselOptions.autoplay = false;
    owlCarouselOptions.responsive = {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        1000: {
            items: 5
        }
    };
    $('.slider-3').owlCarousel(owlCarouselOptions);

});