    function setPageNumber(pageNumber) {
        $(".page-number").text(' strona: ' + pageNumber).css("font-weight", "500");
    } 
$(document).ready(function() {
    const currentPage = window.location.href.split('?page=');
    const pagination = ".pagination li";

    if (currentPage[1]) {
        // $(".page-number").text(' strona: ' + currentPage[1]).css("font-weight", "500");
        setPageNumber(currentPage[1]);
    } else {
        $(".page-number").text(' strona: ' + "1");
        $('li[data-number="1"]').addClass("active");
    }
    $(pagination + '[data-number="' + currentPage[1] + '"]').addClass('active');
    $(pagination).on('click', function(e) {
        e.preventDefault();
        const active = $(this).hasClass("active");
        const siblings = $(this).siblings("li");
        const number = $(this).data('number');
        if (!number) {
            return;
        }
        if (active) {
            siblings.removeClass("active");
        } else {
            $(this).addClass("active");
            siblings.removeClass("active");
            window.location.href = '?page=' + number;
        }
         setPageNumber(number);
    });
});