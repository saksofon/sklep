$(document).ready(function() {
    const minusIcon = "fa-minus";
    const plusIcon = "fa-plus";
    $(".question a").on('click', function() {
        const icon = $(this).siblings("i");
        const isActive = icon.hasClass(minusIcon);
        if (isActive) {
            icon.addClass(plusIcon);
            icon.removeClass(minusIcon);
        } else {
            icon.addClass(minusIcon);
            icon.removeClass(plusIcon);
        }
    });
    $(".question i").on('click', function() {

        const isActive = $(this).hasClass(minusIcon);
        if (isActive) {
            $(this).removeClass(minusIcon);

        } else {
            $(this).addClass(minusIcon);
        }
    });
    $(".input-invisible i").on('click', function(e) {
        e.preventDefault();
        const visibility = $('input').hasClass('visible');
        if (visibility) {
            $('input').addClass('invisible');
            $('input').removeClass('visible');
        } else {
            $('input').addClass('visible');
            $('input').removeClass('invisible');
        }
    });
    $(".menu input").on('keyup', function(e) {
        const currentPage = window.location.href.split('?');
        e.preventDefault();
        const value = document.getElementById("myText").value;
        window.location.href = currentPage[0] + '?name' + value;
    });


    // const textElements = $(".popular-products .box .title");

    // const slice = 7;
    // for(i = 0; i < textElements.length; i++){
    // const textElement = $(textElements[i]); 
    //  console.log(textElement);
    //  const text = textElement.text();

    //  if(slice < text.length){
    //     textElement.text(text.slice(0,slice) + "...");

    //  }
    // }

    setPageNumber = function() {
        const textElements = $(".popular-products .box .title");
        var slice = 7;
        for (i = 0; i < textElements.length; i++) {
            const textElement = $(textElements[i]);
            console.log(textElement);
            const text = textElement.text();
            if (slice < text.length) {
                textElement.text(text.slice(0, slice) + "...");
            }
            const BREAKPOINT = {
                small: 300,
                medium: 640,
                large: 1024,
                xlarge: 1200,
                xxlarge: 1450
            };
            const width = window.innerWidth;
            if (BREAKPOINT.small > width) {
                console.log("if");
                slice = 3;
            } else if (BREAKPOINT.small && BREAKPOINT.medium > width) {
                console.log("small");
                slice = 1;
            } else if (BREAKPOINT.medium && BREAKPOINT.large > width) {
                console.log("medium");
                slice = 5;
            } else if (BREAKPOINT.large && BREAKPOINT.xlarge > width) {
                console.log("large");
                slice = 5;

            } else if (BREAKPOINT.xlarge && BREAKPOINT.xxlarge > width) {
                console.log("xlarge");
                const slice = 5;
                textElement.text(text.slice(0, slice) + "...");
            } else {
                slice = 14;
                console.log("else najwyższa rozdzielczośc");
            }
            textElement.text(text.slice(0, slice) + ">>>>");
        }
    };


    // setPageNumber = function(){
    //     const textElements = $(".popular-products .box .title");

    //    var slice = 7;
    //     for (i = 0; i < textElements.length; i++) {
    //         const textElement = $(textElements[i]);
    //         console.log(textElement);
    //         const text = textElement.text();

    //         if (slice < text.length) {
    //             textElement.text(text.slice(0, slice) + "...");

    //         }
    //         const BREAKPOINT = {
    //             small: 300,
    //             medium: 640,
    //             large: 1024,
    //             xlarge: 1200,
    //             xxlarge: 1450
    //         };

    //        const width = window.innerWidth;
    //         


    //         if (BREAKPOINT.small > width) {
    //             console.log("if");

    //            slice = 3;

    //         } else if (BREAKPOINT.small && BREAKPOINT.medium > width) {
    //             console.log("small");
    //             slice = 1;

    //         } else if (BREAKPOINT.medium && BREAKPOINT.large > width) {
    //             console.log("medium");
    //             const slice = 5;

    //         } else if (BREAKPOINT.large && BREAKPOINT.xlarge > width) {
    //             console.log("large");
    //           slice = 5;

    //         } else if (BREAKPOINT.xlarge && BREAKPOINT.xxlarge > width) {
    //             console.log("xlarge");
    //             const slice = 5;
    //             textElement.text(text.slice(0, slice) + "...");
    //         } else {
    //              slice = 14;

    //             console.log("else najwyższa rozdzielczośc");

    //         }
    //          textElement.text(text.slice(0, slice) + "...");
    //     }

    // };

    // setPageNumber =
    //  $(window).resize .setPageNumber;


}); // zakonczenie document ready